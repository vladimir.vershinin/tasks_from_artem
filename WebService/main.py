from flask import Flask
import datetime
import sqlalchemy as db
from sqlalchemy.engine.url import URL
from sqlalchemy import create_engine
from sqlalchemy import select, column, func, table


app = Flask(__name__)

config = {
    # 'host': 'localhost',
    'host': 'mysql',
    'port': 3306,
    'user': 'vvv',
    'password': '123',
    'database': 'mybase'
}


@app.route("/")
def get_time():
    return "1Time " + datetime.datetime.now().strftime("%d.%m.%Y %H:%M:%S")


@app.route("/sql")
def get_sql():
    db_user = config.get('user')
    db_pwd = config.get('password')
    db_host = config.get('host')
    db_port = config.get('port')
    db_name = config.get('database')
    connection_str = f'mysql+pymysql://{db_user}:{db_pwd}@{db_host}:{db_port}/{db_name}'
    engine = db.create_engine(connection_str)
    connection = engine.connect()
    result = connection.execute("show tables from mybase")
#     connection.execute("""
#     create table employee (
#         emp_id integer primary key,
#         emp_name varchar
#     )
# """)

    if connection:
        # result = connection.execute('select * from mytable;')
        return str(connection)
    else:
        return 'no'


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080)
